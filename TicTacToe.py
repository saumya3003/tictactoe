def display_board(board):

    print('        |       |     ')
    print('  ',  board[0],'   |  ',board[1],'  |   ',board[2])
    print('        |       |     ')
    print('--------------------------')
    print('        |       |     ')
    print('  ',  board[3],'   |  ',board[4],'  |   ',board[5])
    print('        |       |     ')
    print('--------------------------')
    print('        |       |     ')
    print('  ',  board[6],'   |  ',board[7],'  |   ',board[8])
    print('        |       |     ')


def player_input():
    marker= None
    while marker not in ('X','O'):
        marker=input('Choose a marker for you(X/O) : ')
        marker=marker.upper()
    return marker    
    

def place_marker(board, marker, position):
    board[position]=marker

def win_check(board,mark):
    if (board[0]==mark and board[1]==mark and board[2]==mark) or (board[3]==mark and board[4]==mark and board[5]==mark) or (board[6]==mark and board[7]==mark and board[8]==mark) or (board[0]==mark and board[3]==mark and board[6]==mark) or (board[1]==mark and board[4]==mark and board[7]==mark) or (board[8]==mark and board[5]==mark and board[2]==mark) or (board[0]==mark and board[4]==mark and board[8]==mark) or (board[6]==mark and board[4]==mark and board[2]==mark):
        
        return True
    else:
      return False
        

import random
def choose_first():
    a=random.randrange(0,2)
    if a==0:
        return 'Person 1'
    else:
        return 'Person 2'
    
def space_check(board, position):
    if board[position]==' ':
         return True
    return False

def full_board_check(board):
    if ' ' in board:
        return False
    else:
        return True
    
def player_choice(board):
    position=int(input('Enter the position(1-9): '))
    if 0<position<10:
      if (space_check(board, position-1)):
          return position-1
      else:
        return 'Invalid'
    else:
        return 'Invalid'  


def replay():
    i=input('Do you want to play again(Y/N): ' )
    if i.upper()=='Y':
        return True
    else:
        return False


print('Welcome to Tic Tac Toe!')
print('Note the positions below')
board=[1,2,3,4,5,6,7,8,9]
display_board(board)
print()
print('Lets Start')
print()
while True:
    board=[' ']*9
    # Set the game up here
    #pass
    display_board(board)
    First_turn=choose_first()
    print()
    print(First_turn,'its your turn first')
    print()
    marker=player_input()
    
    if First_turn=='Person 1' and marker=='X':
      Second_turn='Person 2'
      marker2='O'
    elif First_turn=='Person 2' and marker=='X':
      Second_turn='Person 1'
      marker2='O'
    elif First_turn=='Person 2' and marker=='O':
      Second_turn='Person 1'
      marker2='X'
    else:
      Second_turn='Person 2'
      marker2='X'
    print()
    print(First_turn+' : '+marker+' and '+Second_turn +' : '+marker2)
    print()  
    
    while True:
        #Player 1 Turn
        print(First_turn)
        position=player_choice(board)
        while position=='Invalid':
          if position=='Invalid':
            print()
            print('Oops!! Wrong position. Choose a position that is empty/valid')
            position=player_choice(board)
        place_marker(board, marker, position)
        print()
        display_board(board)
        print()
        if(win_check(board,marker)):
          print()
          print (First_turn+' won the game!!!')
          break
        if (full_board_check(board)):
          print()
          print('Game Draw')
          break
        
        # Player2's turn.
        print(Second_turn)
        position=player_choice(board)
        while position=='Invalid':
          if position=='Invalid':
            print()
            print('Oops!! Wrong position. Choose a position that is empty/valid')
            position=player_choice(board)
        place_marker(board, marker2, position)
        print()
        display_board(board)
        print()
        if(win_check(board,marker2)):
          print()
          print (Second_turn+' won the game!!!')
          break
        
        if (full_board_check(board)):
          print()
          print('Game Draw')
          break
            
            #pass
    print()
    if not replay():
        break