def display_board(board):

    print('        |       |     ')
    print('  ',  board[0],'   |  ',board[1],'  |   ',board[2])
    print('        |       |     ')
    print('--------------------------')
    print('        |       |     ')
    print('  ',  board[3],'   |  ',board[4],'  |   ',board[5])
    print('        |       |     ')
    print('--------------------------')
    print('        |       |     ')
    print('  ',  board[6],'   |  ',board[7],'  |   ',board[8])
    print('        |       |     ')

import random
def choose_first():
    a=random.randrange(0,2)
    if a==0:
        return 'Player'
    else:
        return 'Computer'
    
def space_check(board, position):
    if board[position]==' ':
         return True
    return False

def player_input(board):
    try:
        position=int(input('Enter the position(1-9): '))
        if 0<position<10:
          if (space_check(board, position-1)):
              return position-1
          else:
            return 'Invalid'
        else:
            return 'Invalid'
    except:
        return 'Invalid'
        

def place_marker(board, marker, position):
    board[position]=marker

def win_check(board,mark):
    if (board[0]==mark and board[1]==mark and board[2]==mark) or (board[3]==mark and board[4]==mark and board[5]==mark) or (board[6]==mark and board[7]==mark and board[8]==mark) or (board[0]==mark and board[3]==mark and board[6]==mark) or (board[1]==mark and board[4]==mark and board[7]==mark) or (board[8]==mark and board[5]==mark and board[2]==mark) or (board[0]==mark and board[4]==mark and board[8]==mark) or (board[6]==mark and board[4]==mark and board[2]==mark):
        return True
    else:
      return False
        


def full_board_check(board):
    if ' ' in board:
        return False
    else:
        return True
    
def player_choice():
    marker= None
    while marker not in ('X','O'):
        marker=input('Choose a marker for you(X/O) : ')
        marker=marker.upper()
    if marker=='X':
        return ('X','O')
    else:
      return ('O','X')

def replay():
    i=input('Do you want to play again(Y/N): ' )
    if i.upper()=='Y':
        return True
    else:
        return False

def winning_probability(board,mark):
    #print('winning_probability')
    #print(board)
    for position in range(1,10):
        board_copy=[]
        for i in board:
            board_copy.append(i) 
        if space_check(board_copy, position-1):
    		    board_copy[position-1]=mark
    		    if (win_check(board_copy,mark)):
    		        #print(position-1)
    		        return position-1
    		        
    			    
def double_check(board,mark):
    #print('double_check')
    #print(board)
    for position in range(1,10):
  	    board_copy=[]
  	    for a in board:
  	        board_copy.append(a)
  	    if space_check(board_copy, position-1):
      			board_copy[position-1]=mark
      			win_in_moves=0
      			#print(board_copy,win_in_moves)
      			for i in range(1,10):
      			    board_copy_copy=[]
      			    for b in board_copy:
      			        board_copy_copy.append(b)
      			    #print(i)
      			    if (space_check(board_copy_copy, i-1)) :
      			        board_copy_copy[i-1]=mark
      			        #print(board_copy_copy)
      			        if win_check(board_copy_copy,mark):
      					        win_in_moves=+1
      					        #print('win_in_moves',win_in_moves)
      			return win_in_moves>=2
      			

def double_check_position(board,mark):
    #print('double_check')
    #print(board)
    for position in range(1,10):
  	    board_copy=[]
  	    for a in board:
  	        board_copy.append(a)
  	    if space_check(board_copy, position-1):
      			board_copy[position-1]=mark
      			win_in_moves=0
      			#print(board_copy,win_in_moves)
      			for i in range(1,10):
      			    board_copy_copy=[]
      			    for b in board_copy:
      			        board_copy_copy.append(b)
      			    #print(i)
      			    if (space_check(board_copy_copy, i-1)) :
      			        board_copy_copy[i-1]=mark
      			        #print(board_copy_copy)
      			        if win_check(board_copy_copy,mark):
      					        win_in_moves=+1
      					        #print(win_in_moves)
      			return position-1      			
      										
def chooseRandomMove(board, movesList):
    # Returns a valid move from the passed list on the passed board.
    # Returns None if there is no valid move.
    possibleMoves = []
    for i in movesList:
        if space_check(board, i):
            possibleMoves.append(i)

    if len(possibleMoves) != 0:
        return random.choice(possibleMoves)
    else:
        return None
  


print('Welcome to Tic Tac Toe!')
print('Note the positions below')
board=[1,2,3,4,5,6,7,8,9]
display_board(board)
print()
print('Lets Start')
print()
Second_turn='Unknown'
while True:
    board=[' ']*9
    # Set the game up here
    #pass
    display_board(board)
    player_mark,computer_mark=player_choice()
    print('Your marker : '+player_mark+' and Computer\'s marker' +' : '+computer_mark)
    First_turn=choose_first()
    print()
    print(First_turn,'its your turn first')
    print()
    
    while True:
        if First_turn=='Player':
            print(First_turn)
            position=player_input(board)
            while position=='Invalid':
              if position=='Invalid':
                  print()
                  print('Oops!! Wrong position. Choose a position that is empty/valid')
                  position=player_input(board)
            place_marker(board, player_mark, position)
            print()
            display_board(board)
            print()
            if(win_check(board,player_mark)):
                print()
                print (First_turn+' won the game!!!')
                break
            if (full_board_check(board)):
                print()
                print('Game Draw')
                break
            else:
                First_turn='Computer'


        else:
            print('Computer\'s Turn')
            #checking the winning_probability of computer in one move
            if (winning_probability(board,computer_mark)!=None):
                #print('winning_probability check for comp')
                pos=winning_probability(board,computer_mark)
                place_marker(board, computer_mark, pos) 
                display_board(board)
                if win_check(board,computer_mark): 
                    display_board(board)
                    print('Computer won the game')
                    break
                  
            #checking the winning_probability of player and making it unsuccessful
            elif (winning_probability(board,player_mark)!=None):
                #print('winning_probability check for player')
                pos=winning_probability(board,player_mark)
                place_marker(board, computer_mark, pos)   
                display_board(board)
                      
            #checking the double move of computer
            elif double_check(board,computer_mark):
                #print('double_check for Comp')
                pos=double_check_position(board,computer_mark)
                print(pos)
                place_marker(board, computer_mark, pos)   
                display_board(board)
                      
            #checking the double move of player
            elif double_check(board,player_mark):
                #print('double_check for Comp')
                pos=double_check_position(board,player_mark)
                print(pos)
                place_marker(board, computer_mark, pos)   
                display_board(board)  
        
            else:
                #Corner
                if (chooseRandomMove(board,[0,2,6,8]))!=None:
                    #print('corner')
                    pos=chooseRandomMove(board,[0,2,6,8])
                    place_marker(board, computer_mark, pos)
                    display_board(board)
                elif(space_check(board,4)):
                    #center
                    #print('Center')
                    place_marker(board, computer_mark, 4)
                    display_board(board)
                    #side
                elif (chooseRandomMove(board,[1,3,5,7]))!=None:
                    #print('Sides')
                    pos=chooseRandomMove(board,[1,3,5,7])
                    place_marker(board, computer_mark, pos)
                    display_board(board)
            if (full_board_check(board)):
                print()
                print('Game Draw')
                break
                        
                        
            First_turn='Player'


    print()
    if not replay():
        break